
window.onload = function () {
    let doctor_id = localStorage.getItem("doctor_id");

    if (doctor_id != null) {
        get_data_from_api(doctor_id);
    }
    else {
        get_data_from_api(1);
    }
}

function redirect_to_login() {
    window.location.href = "login.html";
}

function get_data_from_api(doctor_id) {
    let url = "https://intense-ravine-40625.herokuapp.com/doctors/"+doctor_id;

    // Call the fetch function passing the url of the API as a parameter
    fetch(url)
        .then((resp) => resp.json()) // Transform the data into json
        .then( async function (data) {

            let id = data['id'];
            let name = data['name'];
            let spec = data['spec'];
            let avatar = data['avatar'];
            let number = data['number'];
            let online_pay = data['online_pay'];
            let first_empty_date = data['first_empty_date'];
            let experience_years = data['experience_years'];
            let stars = data['stars'];
            let rate = data['rate'];
            let commenter = data['commenter'];
            let comments = data['comments'];
            let comment_text = data['comment_text'];
            let address = data['address'];
            let phone = data['phone'];
            let week_days = data['week_days'];

            bredcrum_spec = document.getElementById("way-2");
            bredcrum_spec.innerText = spec + " >";

            bredcrum_name = document.getElementById("way-3");
            bredcrum_name.innerText = name;

            right_parent = document.getElementById("middle-Part-drInfo-top-middleColumn");

            let avatar_img = document.createElement("img");
            avatar_img.src = avatar;

            let name_h1 = document.createElement("h1");
            name_h1.innerText = name;

            let spec_p = document.createElement("p");
            spec_p.innerText = spec;

            let number_p = document.createElement("p");
            number_p.innerText = "شماره پرونده : " + number;

            right_parent.appendChild(avatar_img);
            right_parent.appendChild(name_h1);
            right_parent.appendChild(spec_p);
            right_parent.appendChild(number_p);

            let experience_p = document.getElementById("experience");
            experience_p.innerText = experience_years + "سال";

            let empty_date_p = document.getElementById("first-empty-date");
            empty_date_p.innerText = first_empty_date;

            let online_p;
            if (online_pay)
            {
                online_p = "دارد";
            }
            else {
                online_p = "ندارد";
            }

            let online_pay_p = document.getElementById("online-pay");
            online_pay_p.innerText = online_p;

            let rates_p = document.getElementById("rate");
            rates_p.innerText = rate;



            let star_svg = document.getElementsByClassName("feather-star");
            for (let i=0; i < stars; i++){
                star_svg[i].style.fill = 'blue';
            }

            let comments_number = document.getElementById("comments-number");
            comments_number.innerText = "از" + comments + "نظر";

            let commenter_p = document.getElementById("commenter");
            commenter_p.innerText = "نظر " + commenter;

            let comment_p = document.getElementById("comment");
            comment_p.innerText = comment_text;

            let address_p = document.getElementById("address");
            address_p.innerText = address;

            let phone_p = document.getElementById("phone");
            phone_p.innerText = phone;


            let days = document.getElementsByClassName("day");
            for (let i=0; i<7; i++)
            {
                    if (!week_days[i])
                    {
                         days[i].getElementsByClassName("tik")[0].classList.remove("appear");
                         days[i].getElementsByClassName("tik")[0].classList.add("hidden");

                         days[i].getElementsByClassName("untik")[0].classList.remove("hidden");
                         days[i].getElementsByClassName("untik")[0].classList.add("appear");

                    }
                    else {
                         days[i].getElementsByClassName("tik")[0].classList.remove("hidden");
                         days[i].getElementsByClassName("tik")[0].classList.add("appear");

                         days[i].getElementsByClassName("untik")[0].classList.remove("appear");
                         days[i].getElementsByClassName("untik")[0].classList.add("hidden");
                    }
            }

            let matab_title = document.getElementById("middle-Part-loaction-title1");
            let days_on_title = document.getElementById("middle-Part-loaction-title2");


            matab_title.onclick = function () {
                    matab_title.style.background = "white";
                    days_on_title.style.background = "#ededed";

                    let matab = document.getElementById("middle-Part-loaction-content");

                    matab.classList.remove("hidden");
                    matab.classList.add("appear");

                    let days_on = document.getElementById("middle-Part-loaction-content-days");

                    days_on.classList.remove("appear");
                    days_on.classList.add("hidden");

            };


            days_on_title.onclick = function () {
                    matab_title.style.background = "#ededed";
                    days_on_title.style.background = "white";

                    let matab = document.getElementById("middle-Part-loaction-content");

                    matab.classList.remove("appear");
                    matab.classList.add("hidden");

                    let days_on = document.getElementById("middle-Part-loaction-content-days");

                    days_on.classList.remove("hidden");
                    days_on.classList.add("appear");

            };



        })
        .catch((rejected)=>window.alert(rejected))
}
