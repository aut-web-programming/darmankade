let most_satisfy_required=false;

window.onload = function (){
    localStorage.setItem("doctor_id",1);
}

function redirect_to_login() {
    window.location.href = "login.html";
}

// Get data from api with page
function get_data_from_api() {
    let url = "https://intense-ravine-40625.herokuapp.com/doctors";

    // Call the fetch function passing the url of the API as a parameter
    fetch(url)
        .then((resp) => resp.json()) // Transform the data into json
        .then(async function (data) {
            document.querySelectorAll('.specialist-container').forEach(e => e.remove());

            if (most_satisfy_required===true){
                let sortedData = data.sort(function (a, b) {
                        return b['user_percent'] - a['user_percent'];
                    }
                );

                data = sortedData;
            }

            for (let key in data) {
                let id = data[key]['id'];
                let name = data[key]['name'];
                let spec = data[key]['spec'];
                let avatar = data[key]['avatar'];
                let stars = data[key]['stars'];
                let comments = data[key]['comments'];
                let comment_text = data[key]['comment_text'];
                let location = data[key]['location'];
                let experience_years = data[key]['experience_years'];
                let user_percent = data[key]['user_percent'];
                let first_empty_data = data[key]['first_empty_date'];

                let parent = document.getElementById("specializations-container");
                let parent_last_child = document.getElementById("specializations-container-page");

                let element = document.createElement("div");
                element.setAttribute("class", "specialist-container row w-100 mb-3 bg-white p-3 border-x-light-gray rounded-10 hover-border-orange");
                parent.insertBefore(element, parent_last_child);


                let element_div_image = document.createElement("div");
                element_div_image.setAttribute("class", "px-0 pl-sm-3 col-2 w-75 h-75");
                element.appendChild(element_div_image);

                let element_img_dr = document.createElement("img");
                element_img_dr.setAttribute("class","w-75 h-75 border-radius-50");
                element_img_dr.setAttribute("src", avatar);
                element_div_image.appendChild(element_img_dr);

                let element_div_main = document.createElement("div");
                element_div_main.setAttribute("class","pr-md-0 pr-4 pt-2 col-4");
                element.appendChild(element_div_main);

                let element_h5_name = document.createElement("h5");
                element_h5_name.setAttribute("class", "font-weight-bold d-flex text-18");
                element_h5_name.innerHTML = name;
                element_div_main.appendChild(element_h5_name);

                let element_h6_spec = document.createElement("h6");
                element_h6_spec.setAttribute("class", "mb-0 font-weight-normal d-flex text-16");
                element_h6_spec.innerHTML = String(spec);
                element_div_main.appendChild(element_h6_spec);

                let element_div_stars = document.createElement("div");
                element_div_stars.setAttribute("class","my-md-4 mt-2 mb-3 d-flex offset-3");

                let element_img_star = document.createElement("img");
                element_img_star.setAttribute("src", "./images/specialist/star.png");
                element_div_stars.appendChild(element_img_star);

                element_img_star = document.createElement("img");
                element_img_star.setAttribute("src", "./images/specialist/star.png");
                element_div_stars.appendChild(element_img_star);

                element_img_star = document.createElement("img");
                element_img_star.setAttribute("src", "./images/specialist/star.png");
                element_div_stars.appendChild(element_img_star);

                element_img_star = document.createElement("img");
                element_img_star.setAttribute("src", "./images/specialist/star.png");
                element_div_stars.appendChild(element_img_star);

                element_img_star = document.createElement("img");
                element_img_star.setAttribute("src", "./images/specialist/star.png");
                element_div_stars.appendChild(element_img_star);

                let element_span_comments = document.createElement("span");
                element_span_comments.innerText = "(" + comments + "نظر )" ;
                element_div_stars.appendChild(element_span_comments);
                element_div_main.appendChild(element_div_stars);


                let element_p_comment = document.createElement("p");
                element_p_comment.setAttribute("class","text-14 mb-0 text-right");
                element_p_comment.innerText = comment_text;
                element_div_main.appendChild(element_p_comment);

                let element_div_left = document.createElement("div");
                element_div_left.setAttribute("class","px-0 pr-md-4 col-md-5 col-4");
                element.appendChild(element_div_left);

                let element_div_blue = document.createElement("div");
                element_div_blue.setAttribute("class","color-x-gray card border-0 bg-x-ice-blue rounded-10 p-3 align-items-start");
                element_div_left.appendChild(element_div_blue);

                let element_div_location = document.createElement("div");
                element_div_location.setAttribute("class", "py-1 font-weight-bold");
                element_div_blue.appendChild(element_div_location);

                let element_img_location = document.createElement("img");
                element_img_location.setAttribute("src","./images/specialist/location.png");
                element_div_location.appendChild(element_img_location);

                let element_span_location = document.createElement("span");
                element_span_location.setAttribute("class","mr-3");
                element_span_location.innerText = location;
                element_div_location.appendChild(element_span_location);

                let element_div_experience = document.createElement("div");
                element_div_blue.appendChild(element_div_experience);

                let element_img_experience = document.createElement("img");
                element_img_experience.setAttribute("src","./images/specialist/book.png");
                element_div_experience.appendChild(element_img_experience);

                let element_span_experience = document.createElement("span");
                element_span_experience.setAttribute("class", "mr-3");
                element_span_experience.innerText = "تجربه کاری " + experience_years + " سال ";
                element_div_experience.appendChild(element_span_experience);

                let element_div_like = document.createElement("div");
                element_div_blue.appendChild(element_div_like);

                let element_img_like = document.createElement("img");
                element_img_like.setAttribute("src","./images/specialist/like.png");
                element_div_like.appendChild(element_img_like);

                let element_span_like = document.createElement("span");
                element_span_like.setAttribute("class","mr-3");
                element_span_like.innerText = "رضایت کاربران " + user_percent + "درصد  ";
                element_div_like.appendChild(element_span_like);

                let element_div_left_bottom = document.createElement("div");
                element_div_left.appendChild(element_div_left_bottom);

                let element_div_reserve = document.createElement("div");
                element_div_reserve.setAttribute("class", "grid-3-s-l mb-2 d-inline-flex w-100 justify-content-between");
                element_div_left_bottom.appendChild(element_div_reserve);

                let element_button_reserve = document.createElement("button");
                element_button_reserve.setAttribute("class", "text-16 text-center px-5 w-100 py-3 mt-4 button-x-orange font-weight-bold");
                element_button_reserve.innerText= "نوبت بگیر!";
                element_button_reserve.onclick= function() {redirect_to_doctor(id)};
                element_div_reserve.appendChild(element_button_reserve);

                let element_button_like = document.createElement("button");
                element_button_like.setAttribute("class", "mt-4 w-25 no-border bg-x-gray d-flex align-items-center");
                element_div_reserve.appendChild(element_button_like);

                let element_img_heart = document.createElement("img");
                element_img_heart.setAttribute("class", "mr-2 pr-3 img-fluid");
                element_img_heart.setAttribute("src", "images/specialist/heart.png");
                element_button_like.appendChild(element_img_heart);

                let element_p_empty = document.createElement("p");
                element_p_empty.setAttribute("class", "font-weight-normal mb-0 color-x-green text-center");
                element_p_empty.innerText = "اولین نوبت خالی " + first_empty_data;
                element_div_left_bottom.appendChild(element_p_empty);

            }
        })
        .catch((rejected)=>window.alert(rejected))
}


function redirect_to_doctor(id){
//    TODO: Handle "id"
    localStorage.setItem("doctor_id",id);
    window.location.href = "doctor.html";
}

function selected_button_click(){
    most_satisfy_required=false;
    let element_selected = document.getElementById("selected_button");
    let element_most_satisfy = document.getElementById("most_satisfy_button");
    element_selected.setAttribute("class", "col-1 font-weight-normal text-14 pointer-rounded-10 no-border bg-x-blue text-white m-auto rounded-10 py-2");
    element_most_satisfy.setAttribute("class", "col-1 font-weight-normal text-14 pointer-rounded-10 p-1 px-2 m-auto no-border bg-white color-x-gray");

    get_data_from_api();
}

function most_satisfy_button_click(){
    most_satisfy_required=true;
    let element_selected = document.getElementById("selected_button");
    let element_most_satisfy = document.getElementById("most_satisfy_button");
    element_most_satisfy.setAttribute("class", "col-1 font-weight-normal text-14 pointer-rounded-10 no-border bg-x-blue text-white m-auto rounded-10 py-2");
    element_selected.setAttribute("class", "col-1 font-weight-normal text-14 pointer-rounded-10 p-1 px-2 m-auto no-border bg-white color-x-gray");

    get_data_from_api();
}

get_data_from_api();