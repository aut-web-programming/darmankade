function redirect_to_login() {
    window.location.href = "login.html";
}

function redirect_to_medicalSpecialists() {
    window.location.href = "medicalSpecialists.html";
}

function redirect_to_specialist() {
    window.location.href = "specialist.html";
}